from django.test import TestCase
from app01 import models

# Create your tests here.

class UserTestCase(TestCase):

    def setUp(self):
        models.User.objects.create(username='user01',password='passwd1',email='email01@rest.com')
        models.User.objects.create(username='user02',password='passwd2',email='email02@rest.com')

    def test_animals_can_speak(self):
        lion = models.User.objects.get(username="user01")
        cat = models.User.objects.get(username="user02")
        self.assertEqual(lion, 'The lion says "roar"')
        self.assertEqual(cat, 'The cat says "meow"')

