#!/usr/bin/env python
# -* coding: utf-8 -*
# create by cvno on 2018/3/25 14:53
from rest_framework import serializers
from app01 import models


class MySerializer(serializers.Serializer):
    nid = serializers.IntegerField(read_only=True)
    username = serializers.CharField(max_length=64)
    password = serializers.CharField(max_length=32)
    email = serializers.CharField(max_length=32)

    # 和 Form 表单一样
    def validate_username(self, value):
        return value

    def validate_email(self, value):
        return value

    def update(self, instance, validated_data):
        instance.name = validated_data['name']
        instance.user = validated_data['user']
        instance.save()

    def create(self, validated_data):
        models.User.objects.create(**validated_data)
