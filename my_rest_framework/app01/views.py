#!/usr/bin/env python
# -* coding: utf-8 -*
import json
from django.shortcuts import render, HttpResponse
from django.http import JsonResponse
from app01.models import *
from rest_framework.viewsets import ModelViewSet
from rest_framework.serializers import HyperlinkedModelSerializer


# Create your views here.

# 1. django rest framework
class UsersSerializer(HyperlinkedModelSerializer):
    ''' rest api 数据定制 指定显示的数据 '''

    class Meta:
        model = User
        fields = ('nid', 'username')  # 字段
        # exclude = ('password',)    # 除这个字段以外
        depth = 0  # 0 <= depth <= 10   查询外键 0 只查自己的表 1 关联的下一层也会拿到 ...


class UsersViewSet(ModelViewSet):
    queryset = User.objects.all().order_by('-nid')
    serializer_class = UsersSerializer  # 验证数据库操作 相当于 Form的功能


# 2. CBV
from rest_framework.views import APIView
from app01 import serializers
from rest_framework.parsers import JSONParser   # json -> loads

class UserView(APIView):
    def get(self, request, *args, **kwargs):
        """ 获取多条数据 """
        data_list = User.objects.all()

        # 1. django 序列化
        # from django.core import serializers
        # data = serializers.serialize('json',data_list)
        # return HttpResponse(data)

        # 2. rest framework 序列化 + form 验证 + 自定义
        serializer = serializers.MySerializer(instance=data_list, many=True)  # many -> 单条 or 多条
        # return HttpResponse(json.dumps(serializer.data,ensure_ascii=False))   # 正常显示中文
        return JsonResponse(serializer.data,safe=False,json_dumps_params={"ensure_ascii":False})

    def post(self, request, *args, **kwargs):
        """ 创建数据 """
        # old
        # print(request.data)
        # Blog.objects.create(**request.data)
        # rest framework
        data = JSONParser().parse(request)
        serializer = serializers.MySerializer(data=data)
        if serializer.is_valid():
            # print(serializer.data)
            # print(serializer.errors)
            # print(serializer.validated_data)
            # 如果有 instance，则执行 update 方法；否则执行 create
            serializer.save()
        return HttpResponse('...')


class UserDetail(APIView):
    def get(self, request, nid):  # *args,**kwargs ->  pk`
        """ 获取单条数据 """
        obj = User.objects.filter(nid=nid).first()
        serializer = serializers.MySerializer(instance=obj)
        return JsonResponse(serializer.data, safe=False, json_dumps_params={"ensure_ascii": False})

    def put(self, request, nid):  # *args,**kwargs ->  pk`
        ''' 修改单条数据 '''
        obj = User.objects.filter(nid=nid).first()
        data = JSONParser().parse(request)
        serializer = serializers.MySerializer(instance=obj,data=data)
        if serializer.is_valid():
            serializer.save()
            return HttpResponse(200)
        # TODO


    def delete(self, request, nid):  # *args,**kwargs ->  pk`
        """ 删除数据 """
        obj = User.objects.filter(nid=nid).delete()
        return HttpResponse(status=204)
