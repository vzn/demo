#!/usr/bin/env python
# -* coding: utf-8 -*

from django.db import models

# Create your models here.

class User(models.Model):
    nid = models.IntegerField(primary_key=True)
    username = models.CharField(verbose_name='用户名', max_length=32,unique=True)
    password = models.CharField(verbose_name='密码', max_length=64)
    email = models.EmailField(verbose_name='邮箱')

    def __str__(self):
        return self.username

class Blog(models.Model):
    """
    博客信息
    """
    nid = models.BigAutoField(primary_key=True)
    title = models.CharField(verbose_name='个人博客标题', max_length=64)
    site = models.CharField(verbose_name='个人博客前缀', max_length=32, unique=True)
    theme = models.CharField(verbose_name='博客主题', max_length=32)
    user = models.OneToOneField(to='User', to_field='nid')

    def __str__(self):
        return "%s--%s"%(self.title,self.user)