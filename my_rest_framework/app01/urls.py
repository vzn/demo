#!/usr/bin/env python
# -* coding: utf-8 -*
# create by cvno on 2018/3/23 18:55

from rest_framework import routers
from django.conf.urls import url,include
from app01 import views

router = routers.DefaultRouter()
router.register(r'users',views.UsersViewSet) #  rest framework api 路由映射

urlpatterns = [
    # CBV
    url(r'user/$', views.UserView.as_view()),  # CBV
    # url(r'user/(?P<pk>[0-9]+)/$',views.UserDetail.as_view()), # CBV
    url(r'user/(\d+)/$', views.UserDetail.as_view()),  # CBV
    # rest framework api url
    url(r'^',include(router.urls))
]