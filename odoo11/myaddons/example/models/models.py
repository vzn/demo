# -*- coding: utf-8 -*-

from odoo import models, fields, api

# class example(models.Model):
#     _name = 'example.example'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100

class Staff(models.Model):
    _name = 's.user'

    name = fields.Char(required=True)


    # ktime = fields.Integer(string='工时', compute='_check_ktime', )