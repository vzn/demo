# -*- coding: utf-8 -*-

from odoo import models, fields, api

# class firstapp(models.Model):
#     _name = 'firstapp.firstapp'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100
class openacademy(models.Model):
    _name = 'openacademy.openacademy'

    name = fields.Char(required=True)
    # description = fields.Text()

# TODO 定义 model 模型
class Course(models.Model):
    ''' 课程表 '''
    _name = 'openacademy.course'

    name = fields.Char(string="课程标题", required=True)
    description = fields.Text(string="课程描述",)
    responsible_id = fields.Many2one('res.users',
                                     ondelete='set null', string="Responsible | 课程负责人", index=True) # 如果 res.users 删除了一个用户，那么这个字段就设置为空
    session_ids = fields.One2many(
        'openacademy.session', 'course_id', string="Sessions | 课时")


class Session(models.Model):
    ''' 课时表 '''
    _name = 'openacademy.session'   # 表名

    name = fields.Char(required=True)
    start_date = fields.Datetime(string='开始时间')
    duration = fields.Float(digits=(6, 2), help="Duration in days | 课程时长")
    seats = fields.Integer(string="Number of seats | 座位数量")
    # instructor_id = fields.Many2one('res.partner', string="Instructor | 辅导员")   # res.users 可以对应到某一个 res.partner
    instructor_id = fields.Many2one('res.partner', string="Instructor",
    #                                 domain=[('instructor', '=', True)])
                                    domain=['|', ('instructor', '=', True),
                                            ('category_id.name', 'ilike', "Teacher")])# 过滤

    course_id = fields.Many2one('openacademy.course',
                                ondelete='cascade', string="Course | 课程", required=True) # 如果课程表里面的数据删除了，就会把这张表的相关的数据全部删除
    attendee_ids = fields.Many2many('res.partner', string="Attendees | 上课的学生")  # odoo 会自动生成一张表来维护关系，和 Django 一样
    # 计算上课认数的比例 todo
    taken_seats = fields.Float(string="Taken seats", compute='_taken_seats')

    @api.depends('seats', 'attendee_ids')
    def _taken_seats(self):
        for r in self:
            if not r.seats:
                r.taken_seats = 0.0
            else:
                r.taken_seats = 100.0 * len(r.attendee_ids) / r.seats



# 1
class SessioinExtension(models.Model):
    ''' 新的类兼容继承的类 在原有的表上扩展出来一个字段 '''
    _inherit = 'openacademy.session'
    note = fields.Text()

# 2
class SessioinCopy(models.Model):
    ''' 新的类兼容继承的类 在原有的表上扩展出来一个字段 '''
    _name = 'openacademy.session.copy'  # 子表名
    _inherit = 'openacademy.session'    # 继承
    copy = fields.Text()


# 3 委托模型
class Child0(models.Model):
    _name = 'delegation.child0'

    field_0 = fields.Integer()

class Child1(models.Model):
    _name = 'delegation.child1'

    field_1 = fields.Integer()

class Delegating(models.Model):
    _name = 'delegation.parent'

    _inherits = {
        'delegation.child0': 'child0_id',
        'delegation.child1': 'child1_id',
    }

    child0_id = fields.Many2one('delegation.child0', required=True, ondelete='cascade')
    child1_id = fields.Many2one('delegation.child1', required=True, ondelete='cascade')
"""
    record = env['delegation.parent'].create({
        'child0_id': env['delegation.child0'].create({'field_0': 0}).id,
        'child1_id': env['delegation.child1'].create({'field_1': 1}).id,
    })
    record.field_0
    record.field_1
"""